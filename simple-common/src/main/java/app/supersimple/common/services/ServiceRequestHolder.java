package app.supersimple.common.services;

/**
 * Interface ServiceRequestHolder.
 * Is useful when activity and/or fragment instance waits for a request to finish in a Pub-Sub model like implementation
 * When using a reactive RxJava approach this interface becomes obsolete
 *
 * Created by arjan on 14/12/15.
 */
public interface ServiceRequestHolder {
    /**
     * Get the url as String.
     * @return the fetched URL
     */
    String getUrl();
}

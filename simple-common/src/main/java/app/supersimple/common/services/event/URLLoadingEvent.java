package app.supersimple.common.services.event;

/**
 * Class URLLoadingEvent.
 * A superclass for a Pub-Sub bus event
 *
 * Created by arjan on 14/12/15.
 */
public class URLLoadingEvent {
    /**
     * The returned statusCode.
     */
    private int statusCode;

    /**
     * The request URL as a String.
     */
    private String url;

    /**
     * The ServerError object when null no error occurred.
     */
    private ServerError error;

    protected URLLoadingEvent(int code, String uri) {
        this(code, uri, null);
    }

    protected URLLoadingEvent(int code, String uri, ServerError error) {
        this.statusCode = code;
        this.url = uri;
        this.error = error;
    }

    /**
     * Get the statusCode.
     * @return code as int
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Set the statuscode.
     * @param statusCode int
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get the URL as String.
     * @return url
     */
    public String getURL() {
        return url;
    }

    /**
     * Set the URL.
     * @param url URL as String
     */
    public void setURL(String url) {
        this.url = url;
    }

    /**
     * Get error.
     * @return the error or null
     */
    public ServerError getError() {
        return error;
    }

    /**
     * Setting the error object.
     * @param error the error
     */
    public void setError(ServerError error) {
        this.error = error;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        String urlStatusCode = " [URL:" + url + " (" + statusCode + ")]";
        sb.append(urlStatusCode);

        return sb.toString();
    }

    /**
     * Interface ServerError.
     * Holds basic error information to be handled or shown to the end-user when applicable
     */
    public interface ServerError {

        /**
         * The error code.
         * @return Error code
         */
        int getCode();

        /**
         * The technical error message.
         * @return Error message
         */
        String getError();

        /**
         * Error message to show to the End-user.
         * @return A localized error message
         */
        String getUserError();
    }
}

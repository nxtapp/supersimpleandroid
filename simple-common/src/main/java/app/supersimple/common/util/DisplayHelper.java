package app.supersimple.common.util;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by arjan on 02/09/16.
 */
public final class DisplayHelper {
    private DisplayHelper() { }

    /**
     * Get the display size in pixels.
     * @param context the Context
     * @return size in pixels
     */
    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }
}
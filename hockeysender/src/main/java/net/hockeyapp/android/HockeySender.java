package net.hockeyapp.android;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;

import java.util.Date;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * HockeySender is a helper class that produces a translation between the
 * ACRA report and the HockeyApp crash reporting tool.
 */
public class HockeySender implements ReportSender {
    /**
     * HockeyApp API Endpoint.
     */
    private static String BASE_URL = "https://rink.hockeyapp.net/api/2/apps/";
    /**
     * Crashes API Path.
     */
    private static String CRASHES_PATH = "/crashes";

    @Override
    public void send(CrashReportData report) throws ReportSenderException {
        String log = createCrashLog(report);
        String url = BASE_URL + ACRA.getConfig().formKey() + CRASHES_PATH;

        try {
            FormBody.Builder bodyBuilder = new FormBody.Builder()
                    .add("raw", log)
                    .add("userID", report.get(ReportField.INSTALLATION_ID))
                    .add("contact", report.get(ReportField.USER_EMAIL));
            String value  = report.get(ReportField.USER_COMMENT);
            if (value != null) {
                bodyBuilder.add("description", value);
            }
            RequestBody body = bodyBuilder.build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();

            client.newCall(request).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String createCrashLog(CrashReportData report) {
        Date now = new Date();
        StringBuilder log = new StringBuilder();

        log.append("Package: " + report.get(ReportField.PACKAGE_NAME) + "\n");
        log.append("Version: " + report.get(ReportField.APP_VERSION_CODE) + "\n");
        log.append("Android: " + report.get(ReportField.ANDROID_VERSION) + "\n");
        log.append("Manufacturer: " + android.os.Build.MANUFACTURER + "\n");
        log.append("Model: " + report.get(ReportField.PHONE_MODEL) + "\n");
        log.append("Date: " + now + "\n");
        log.append("\n");
        log.append(report.get(ReportField.STACK_TRACE));

        return log.toString();
    }
}

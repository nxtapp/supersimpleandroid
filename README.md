# SuperSimple android library supporting creating Android apps

SuperSimple is a collection of reusable base classes that makes it easier to handle some glue code that handle e.g. ActionBar customization, Google Analytics tracking.
The project has no official releases and/or tagged versions yet. Feel free to contribute where you see fit.

###Usage

In the root `buidle.gradle` file add:  

	allprojects {
		repositories {
			...
			maven { url "https://jitpack.io" }
		}
	}
	
Add as a dependency in the module/app `build.gradle`:  

	dependencies {
		compile 'org.bitbucket.nxtapp.supersimpleandroid:<module>:<Tag|Commit-Hash>'
	}

Or more advanced/customized:  

	compile ('org.bitbucket.nxtapp.supersimpleandroid:simple-ui:<Tag|Commit>') {
        exclude module: 'simple-common'
    }


####Modules

- simple-common
- simple-ui
- simple-testing
- simple-robo (deprecated)
- simple-network  


###Contribute

If you have a reusable component and want to reuse it different projects but don't want to copy/paste? Feel free to add it to one of the existing modules or create a new one if none of the existing modules is applicable.  
For contributions please checkout `git@bitbucket.org:nxtapp/supersimpleandroid.git` and create a feature branch using:  

- [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/)  
- Create tests where appropriate  
- Run `./gradlew test checkstyle assemble` before committing changes on the master branch  

###License

The MIT License. See LICENSE.txt

###Changelog

0.1.0 First Release  

0.1.3 Back to Java 1.7 (Android is not ready for 1.8)  

0.1.4 Java 1.8 Support  

0.1.5 Simple-ui updates:  
- Test for BitmapHelper  
- SquareImageView Api > 21 support, snapToWidth attribute for customization  
- FontTextView
- DisplayHelper
- ViewPager
- DrawableHelper
- RoundedImageView

0.1.6 Sliding layouts
- SlidingRelativeLayout
- SlidingFrameLayout
- SlidingLinearLayout

##Resources
Third party recourses used in this project:  

- Roboguice (deprecated)
- Otto (messaging bus) (deprecated)
- ACRA
- play-services-analytics
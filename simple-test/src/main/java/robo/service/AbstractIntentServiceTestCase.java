package robo.service;

import android.app.Application;
import android.app.IntentService;
import android.content.Context;

import com.google.inject.Injector;
import com.google.inject.Module;
import com.squareup.otto.Bus;

import org.junit.After;
import org.junit.Before;
import org.robolectric.RuntimeEnvironment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import roboguice.RoboGuice;

/**
 * Abstract TestCase for IntentService objects.
 *
 * Created by arjan on 14/12/15.
 *
 * @param <T> The IntentService class to test
 */
public abstract class AbstractIntentServiceTestCase<T extends IntentService> {
    /**
     * The Event bus for Pub-Sub pattern IntentService testing.
     */
    private Bus eventBus;

    /**
     * Mock Http server that intercepts http request when fired upon the baseUrl.
     */
    protected MockWebServer server;

    /**
     * The Context.
     */
    private Context context;

    /**
     * Local path where test resources can be found.
     * Place test resources in project/src/test/resources/
     */
    protected String resourcePath;

    /**
     * The hostname and port for Mock http requests.
     */
    private String baseUrl;

    /**
     * The created IntentService.
     */
    private T service;

    /**
     * Get the IntentService to fire the test on.
     * @return The desired service
     */
    public T getService() {
        if (service == null) {
            service = createService();
        }
        return service;
    }

    protected abstract T createService();
    protected abstract Module getTestModule();
    protected abstract MockResponse getMockResponse();

    /**
     * Test setup to run before each test*() method.
     * @throws IOException exception
     */
    @Before
    public void setUp() throws IOException {
        //
        // Setup Roboguice
        context = RuntimeEnvironment.application.getBaseContext();
        //
        // Set resource paths
        resourcePath = context.getPackageResourcePath()  + "/src/test/resources/";
        // Create a MockWebServer. These are lean enough that you can create a new
        // instance for every unit test.
        server = new MockWebServer();
        //
        // Set a dispatcher for the requests
        server.enqueue(getMockResponse());
        // Start the server.
        server.start();
        // Ask the server for its URL. You'll need this to make HTTP requests.
        HttpUrl httpBaseUrl = server.url("");
        String strBaseUrl = httpBaseUrl.toString();
        baseUrl = strBaseUrl.substring(0, strBaseUrl.length() - 1);
        System.out.println("baseUrl: " + baseUrl);

        // Override the default RoboGuice module
        Application app = RuntimeEnvironment.application;
        Module module = getTestModule();
        if (module != null) {
            RoboGuice.overrideApplicationInjector(app, module);
        }

        Injector injector = RoboGuice.getInjector(app);
        //
        // 'Inject' our own members.
        eventBus = injector.getInstance(Bus.class);
        service = getService();
        injector.injectMembers(service);

        service.onCreate();
        eventBus.register(this);
    }

    /**
     * Test cleanup.
     * @throws IOException exception
     */
    @After
    public void tearDown() throws IOException {
        service.onDestroy();
        eventBus.unregister(this);
        // Shut down the server. Instances cannot be reused.
        server.shutdown();
        RoboGuice.Util.reset();
        server = null;
        baseUrl = null;
        service = null;
        context = null;
    }

    /**
     * The test Context.
     * @return Context
     */
    public Context getContext() {
        return context;
    }

    /**
     * The base/host URL to be used for request so the MockHttp class can intercept a return test response.
     * @return Hostname and port to use a baseUrl for api/http request
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    protected InputStream getInputStreamFromFile(String filename) throws IOException {
        try {
            InputStream inputStream = new FileInputStream(new File(resourcePath + filename));
            return inputStream;
        } catch (IOException e) {
            System.out.println("Could not load resource: [" + resourcePath + filename + "]");
            throw e;
        }
    }

}

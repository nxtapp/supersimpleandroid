package app.supersimple.ui.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * BitmapHelper utility class for converting Drawable to Bitmap.
 * Created by arjan on 19/01/16.
 */
public final class BitmapHelper {
    private BitmapHelper() {

    }

    /**
     * Convert Drawable interface implementation to concrete Bitmap.
     * @param drawable Drawable to convert
     * @return The created Bitmap
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable != null) {
            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                if (bitmapDrawable.getBitmap() != null) {
                    return bitmapDrawable.getBitmap();
                }
            }

            if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                // Single color bitmap will be created of 1x1 pixel
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                        Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        }
        return bitmap;
    }

    /**
     * Render a Text with a Typeface/font into a Bitmap.
     *
     * @link http://stackoverflow.com/questions/4318572/how-to-use-a-custom-typeface-in-a-widget
     * @param context Context not null
     * @param text The text to put in the Bitmap
     * @param color Text color
     * @param fontSize Text font size
     * @param fontname Text font
     * @return The created Bitmap
     */
    public static Bitmap getFontBitmap(Context context, String text, int color, float fontSize, String fontname) {
        int pad = (int) fontSize / 9;
        Paint paint = new Paint();
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontname);
        paint.setAntiAlias(true);
        paint.setTypeface(typeface);
        paint.setColor(color);
        paint.setTextSize(fontSize);

        int textWidth = (int) (paint.measureText(text) + pad * 2);
        int height = (int) (fontSize / 0.75);
        Bitmap bitmap = Bitmap.createBitmap(textWidth, height, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(bitmap);
        float xOriginal = pad;
        canvas.drawText(text, xOriginal, fontSize, paint);
        return bitmap;
    }
}

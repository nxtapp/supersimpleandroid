package app.supersimple.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by arjan on 29/01/16.
 */
public class ViewPager extends android.support.v4.view.ViewPager {
    /**
     * Constructor.
     * @param context context
     */
    public ViewPager(Context context) {
        super(context);
    }

    /**
     * Constructor
     * @param context context
     * @param attrs Attributes
     */
    public ViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}

package app.supersimple.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import app.supersimple.ui.R;

/**
 * ImageView that snaps to width by default.
 * Attribute snapToWidth (boolean) can alter this behaviour
 *
 * Created by arjan on 19/01/16.
 */
public class SquareImageView extends ImageView {
    /**
     * Default snap to width.
     * Snap to height when false
     * Define through attr SquareImageView_snapToWidth
     */
    private boolean snapToWidth = true;

    /**
     * Default constructor for ImageView.
     * @param context Context not null
     */
    public SquareImageView(Context context) {
        super(context);
    }

    /**
     * Default constructor for ImageVIew.
     * @param context Context not null
     * @param attrs Attributes
     */
    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        consumeAttributes(context, attrs);
    }

    /**
     * Default constructor for ImageView.
     * @param context Context not null
     * @param attrs Attributes
     * @param defStyle Style when defined
     */
    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        consumeAttributes(context, attrs);
    }

    /**
     * Constructor for API > 21.
     * @param context Context not null
     * @param attrs Attributes
     * @param defStyleAttr Style
     * @param defStyleRes Resources
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        consumeAttributes(context, attrs);
    }

    private void consumeAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.app_supersimple_ui_view_SquareImageView,
                0, 0);

        try {
            snapToWidth = a.getBoolean(R.styleable.app_supersimple_ui_view_SquareImageView_snapToWidth, true);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (snapToWidth) {
            setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
        } else {
            setMeasuredDimension(getMeasuredHeight(), getMeasuredHeight()); //Snap to height
        }
    }
}

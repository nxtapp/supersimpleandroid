package app.supersimple.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import app.supersimple.ui.R;

/**
 * TextView subclass that takes a style to set a custom font
 * Fonts should be located in assets/fonts app package
 *
 * Created by arjan on 02/09/16.
 */
public class FontTextView extends TextView {

    /**
     * Constructor.
     * @param context context
     */
    public FontTextView(Context context) {
        super(context);
    }

    /**
     * Contructor.
     * @param context context
     * @param attrs Attributes
     */
    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        consumeAttributes(context, attrs);
    }

    /**
     * Contructor.
     * @param context context
     * @param attrs Attributes
     * @param defStyleAttr Style Attributes definition
     */
    public FontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        consumeAttributes(context, attrs);
    }

    /**
     * Contructor Lollipop and higher.
     * @param context context
     * @param attrs Attributes
     * @param defStyleAttr Style Attributes definition
     * @param defStyleRes Style Attributes resources
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FontTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        consumeAttributes(context, attrs);
    }

    private void consumeAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.app_supersimple_ui_view_FontTextView,
                0, 0);

        try {
            CharSequence fontName = a.getText(R.styleable.app_supersimple_ui_view_FontTextView_font);

            if (fontName != null) {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
                setTypeface(typeface);
            }
        } finally {
            a.recycle();
        }
    }
}

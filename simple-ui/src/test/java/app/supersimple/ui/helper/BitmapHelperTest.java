package app.supersimple.ui.helper;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import app.supersimple.ui.BuildConfig;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by arjan on 20/04/16.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class BitmapHelperTest {

    @Test
    public void testBitmapHelper() {
        ColorDrawable colorDrawable = new ColorDrawable(0xffee33);
        assertNotNull(colorDrawable);

        Bitmap bitmap = BitmapHelper.drawableToBitmap(colorDrawable);
        assertNotNull(bitmap);
    }
}
